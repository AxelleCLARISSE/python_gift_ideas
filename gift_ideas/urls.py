"""gift_ideas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

from app.views.gift_ideas.create import IdeaCreateView
from app.views.gift_ideas.delete import IdeaDeleteView
from app.views.gift_ideas.detail import IdeaDetailView
from app.views.gift_ideas.list_greater_than_100 import IdeaListGreaterThan100View
from app.views.gift_ideas.list_less_than_10 import IdeaListLessThan10View
from app.views.gift_ideas.list_less_than_100 import IdeaListLessThan100View
from app.views.gift_ideas.list_less_than_20 import IdeaListLessThan20View
from app.views.gift_ideas.list_less_than_30 import IdeaListLessThan30View
from app.views.gift_ideas.list_less_than_5 import IdeaListLessThan5View
from app.views.gift_ideas.list_less_than_50 import IdeaListLessThan50View
from app.views.gift_ideas.my_list import IdeaMyListView
from app.views.gift_ideas.list_birthday import IdeaListBirthdayView
from app.views.gift_ideas.list_christmas import IdeaListChristmasView
from app.views.gift_ideas.list_men import IdeaListMenView
from app.views.gift_ideas.list_women import IdeaListWomenView
from app.views.gift_ideas.login import LoginUserView
from app.views.gift_ideas.logout import MyLogoutView
from app.views.gift_ideas.profile import ProfileView
from app.views.gift_ideas.rate import RateView
from app.views.gift_ideas.register import RegisterView
from app.views.gift_ideas.search import IdeaSearchDetail
from app.views.gift_ideas.search_by_description import IdeaSearchByDescriptionView
from app.views.gift_ideas.top_list import IdeaTopListView
from app.views.gift_ideas.update import IdeaUpdateView
from app.views.gift_ideas.update_password import PasswordUpdateView
from app.views.gift_ideas.update_user import UserUpdateView
from app.views.index import IndexView

urlpatterns = [
    path('admin/', admin.site.urls),
]


urlpatterns += i18n_patterns(
    url(r'^$', IndexView.as_view(), name='app_index'),
    url(r'^register/?$', RegisterView.as_view(), name='app_gift_ideas_register'),
    url(r'^login/?$', LoginUserView.as_view(), name='app_gift_ideas_login'),
    url(r'^logout/?$', MyLogoutView.as_view(), name='app_gift_ideas_logout'),
    url(r'^profile/?$', ProfileView.as_view(), name='app_gift_ideas_profile'),
    url(r'^update/?$', UserUpdateView.as_view(), name='app_gift_ideas_update_user'),
    url(r'^upload/?$', RegisterView.as_view(), name='upload'),
    url(r'^update_password/?$', PasswordUpdateView.as_view(), name='app_gift_ideas_update_password'),
    path('ideas/',
         IdeaMyListView.as_view(), name='app_gift_ideas_list'),
    path('ideas/detail/<int:pk>',
         IdeaDetailView.as_view(), name='app_gift_ideas_detail'),
    path('ideas/create',
         IdeaCreateView.as_view(), name='app_gift_ideas_create'),
    path('ideas/update/<int:pk>',
         IdeaUpdateView.as_view(), name='app_gift_ideas_update_idea'),
    path('ideas/delete/<int:pk>',
         IdeaDeleteView.as_view(), name='app_gift_ideas_delete'),
    path('ideas/detail/search/<str:slug>',
         IdeaSearchDetail.as_view(), name='app_gift_ideas_search_detail'),
    path('ideas/search-by-description/<str:slug>',
         IdeaSearchByDescriptionView.as_view(), name='app_gift_ideas_search_by_description'),
    path('ideas/rate/<int:pk>',
         RateView.as_view(), name='app_gift_ideas_rate'),
    path('ideas/categorie/birthday',
         IdeaListBirthdayView.as_view(), name='app_gift_ideas_list_birthday'),
    path('ideas/categorie/christmas',
         IdeaListChristmasView.as_view(), name='app_gift_ideas_list_christmas'),
    path('ideas/categorie/women',
         IdeaListWomenView.as_view(), name='app_gift_ideas_list_women'),
    path('ideas/categorie/men',
         IdeaListMenView.as_view(), name='app_gift_ideas_list_men'),
    path('ideas/top',
         IdeaTopListView.as_view(), name='app_gift_ideas_top_list'),
    path('ideas/less_than_5',
         IdeaListLessThan5View.as_view(), name='app_gift_ideas_top_less_than_5'),
    path('ideas/less_than_10',
         IdeaListLessThan10View.as_view(), name='app_gift_ideas_top_less_than_10'),
    path('ideas/less_than_20',
         IdeaListLessThan20View.as_view(), name='app_gift_ideas_top_less_than_20'),
    path('ideas/less_than_30',
         IdeaListLessThan30View.as_view(), name='app_gift_ideas_top_less_than_30'),
    path('ideas/less_than_50',
         IdeaListLessThan50View.as_view(), name='app_gift_ideas_top_less_than_50'),
    path('ideas/less_than_100',
         IdeaListLessThan100View.as_view(), name='app_gift_ideas_top_less_than_100'),
    path('ideas/greater_than_100',
         IdeaListGreaterThan100View.as_view(), name='app_gift_ideas_top_greater_than_100'),
)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
