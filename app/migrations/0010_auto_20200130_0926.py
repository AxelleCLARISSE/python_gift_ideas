# Generated by Django 2.2.7 on 2020-01-30 09:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0009_auto_20200119_1459'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='note',
            field=models.IntegerField(blank=True, default=None, max_length=1, null=True),
        ),
    ]
