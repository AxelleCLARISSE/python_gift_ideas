# Generated by Django 2.2.7 on 2020-01-30 09:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0010_auto_20200130_0926'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='note',
            field=models.IntegerField(blank=True, default=None, null=True),
        ),
    ]
