from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Categorie(models.Model):
    description = models.CharField(max_length=200, blank=True, null=True, default=None)

    def __str__(self):
        return f'{str(self.description)}'


class Person(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    description = models.TextField(blank=True, null=True, default=True)
    avatar = models.ImageField(upload_to='medias/', null=True, blank=True)

    def __str__(self):
        return '{}'.format(self.user)


class Idea(models.Model):
    title = models.CharField(max_length=200, blank=True, null=True, default='(no title)')
    description = models.CharField(max_length=200, blank=True, null=True, default=None)
    prix = models.FloatField(max_length=10, blank=True, null=True, default=None)
    photo = models.ImageField(upload_to='media/', null=True, blank=True)
    categorie = models.ManyToManyField(Categorie, related_name='ideas', through='IdeaCategorie')
    person = models.ForeignKey(Person, on_delete=models.CASCADE)

    def __str__(self):
        result = self.title
        if self.description is not None:
            result += ' ' + self.description[:80]
        return result


class IdeaCategorie(models.Model):
    idea = models.ForeignKey(Idea, on_delete=models.CASCADE)
    categorie = models.ForeignKey(Categorie, on_delete=models.CASCADE)


class Note(models.Model):
    note = models.PositiveIntegerField(blank=True, null=True, default=None)
    idea = models.ForeignKey(Idea, on_delete=models.CASCADE)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
