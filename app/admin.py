from django.contrib import admin

# Register your models here.
from app.models import Categorie, Person, Idea, IdeaCategorie, Note

admin.site.register(Categorie)
admin.site.register(Person)
admin.site.register(Idea)
admin.site.register(IdeaCategorie)
admin.site.register(Note)
