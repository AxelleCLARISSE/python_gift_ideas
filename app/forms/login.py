from django.forms import widgets, Form
from django import forms


class LoginForm(Form):
    username = forms.CharField(max_length=200, min_length=1,
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control form-control-user',
                                          'placeholder': 'User Name'}))
    password = forms.CharField(max_length=200, min_length=3,
                               widget=widgets.PasswordInput(
                                   attrs={'class': 'form-control form-control-user',
                                          'placeholder': 'Password'}))