from django.forms import models

from app.models import Idea


class IdeaForm(models.ModelForm):
    class Meta:
        model = Idea
        fields = ['title', 'description', 'prix', 'photo', 'categorie']
