from django.contrib.auth.models import User
from django.forms import models
from django import forms


class ProfileForm(models.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name', 'username']

    username = forms.CharField(max_length=200, min_length=1,
                               widget=forms.TextInput(
                                   attrs={'id': 'exampleUsername',
                                          'class': 'form-control form-control-user',
                                          'placeholder': 'User Name'}))
    first_name = forms.CharField(max_length=200, min_length=2,
                                 widget=forms.TextInput(
                                     attrs={'id': 'exampleFirstName',
                                            'class': 'form-control form-control-user',
                                            'placeholder': 'First Name'}))
    last_name = forms.CharField(max_length=200, min_length=2,
                                widget=forms.TextInput(
                                    attrs={'id': 'exampleLastName',
                                           'class': 'form-control form-control-user',
                                           'placeholder': 'Last Name'}))
    email = forms.CharField(max_length=200, min_length=4,
                            widget=forms.TextInput(
                                attrs={'id': 'exampleInputEmail',
                                       'class': 'form-control form-control-user',
                                       'placeholder': 'Email Address'}))

    def clean_username(self):
        username = self.cleaned_data['username']
        if not username.isalnum():
            self.add_error('username', 'Only alphanum chars allowed !')
            return None

        return username
