from django.contrib.auth.models import User
from django.forms import widgets, models
from django import forms


class RegisterForm(models.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'password_1', 'password_2', 'email', 'first_name', 'last_name']

    username = forms.CharField(max_length=200, min_length=1,
                               widget=forms.TextInput(
                                   attrs={'id': 'exampleUsername',
                                          'class': 'form-control form-control-user',
                                          'placeholder': 'User Name'}))
    first_name = forms.CharField(max_length=200, min_length=3,
                                 widget=forms.TextInput(
                                     attrs={'id': 'exampleFirstName',
                                            'class': 'form-control form-control-user',
                                            'placeholder': 'First Name'}))
    last_name = forms.CharField(max_length=200, min_length=3,
                                widget=forms.TextInput(
                                    attrs={'id': 'exampleLastName',
                                           'class': 'form-control form-control-user',
                                           'placeholder': 'Last Name'}))
    email = forms.CharField(max_length=200, min_length=4,
                            widget=forms.TextInput(
                                attrs={'id': 'exampleInputEmail',
                                       'class': 'form-control form-control-user',
                                       'placeholder': 'Email Address'}))
    password_1 = forms.CharField(max_length=200, min_length=4,
                                 widget=widgets.PasswordInput(
                                     attrs={'id': 'examplePassword',
                                            'class': 'form-control form-control-user',
                                            'placeholder': 'Password'}))
    password_2 = forms.CharField(max_length=200, min_length=4,
                                 widget=widgets.PasswordInput(
                                     attrs={'id': 'exampleRepeatPassword',
                                            'class': 'form-control form-control-user',
                                            'placeholder': 'Repeat Password'}))

    def clean_username(self):
        username = self.cleaned_data['username']
        if not username.isalnum():
            self.add_error('username', 'Only alphanum chars allowed !')
            return None
        return username

    def clean(self):
        password_1 = self.cleaned_data['password_1']
        password_2 = self.cleaned_data['password_2']
        if password_1 != password_2:
            self.add_error('password_1', 'Passwords are different !')
            self.add_error('password_2', 'Passwords are different !')
        return super().clean()
