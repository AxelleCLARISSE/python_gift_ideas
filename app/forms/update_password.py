from django.contrib.auth.models import User
from django.forms import models, widgets
from django import forms


class UpdatePasswordForm(models.ModelForm):
    class Meta:
        model = User
        fields = ['password_1', 'password_2']

    password_1 = forms.CharField(max_length=200, min_length=4,
                                 widget=widgets.PasswordInput(
                                     attrs={'id': 'examplePassword',
                                            'class': 'form-control form-control-user',
                                            'placeholder': 'Password'}))
    password_2 = forms.CharField(max_length=200, min_length=4,
                                 widget=widgets.PasswordInput(
                                     attrs={'id': 'exampleRepeatPassword',
                                            'class': 'form-control form-control-user',
                                            'placeholder': 'Repeat Password'}))

    def clean(self):
        password_1 = self.cleaned_data['password_1']
        password_2 = self.cleaned_data['password_2']
        if password_1 != password_2:
            self.add_error('password_1', 'Passwords are different !')
            self.add_error('password_2', 'Passwords are different !')
        return super().clean()