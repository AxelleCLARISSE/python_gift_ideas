from django.forms import models
from app.models import Note


class RateForm(models.ModelForm):
    class Meta:
        model = Note
        fields = ['note']

    def clean_note(self):
        note = self.cleaned_data['note']
        if note > 5:
            self.add_error('note', 'the grade cannot be greater than 5')
            return None
        return note
