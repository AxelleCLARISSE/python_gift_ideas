from django.urls import reverse
from django.views.generic import DeleteView

from app.models import Idea


class IdeaDeleteView(DeleteView):
    template_name = 'idea_delete.html'
    model = Idea

    def get_success_url(self):
        return reverse('app_gift_ideas_list')
