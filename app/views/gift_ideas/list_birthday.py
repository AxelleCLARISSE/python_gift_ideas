from django.views.generic import ListView

from app.models import IdeaCategorie


class IdeaListBirthdayView(ListView):
    template_name = 'birthday.html'
    model = IdeaCategorie

    def get_queryset(self):
        return IdeaCategorie.objects.filter(categorie__description='Anniversaire')


