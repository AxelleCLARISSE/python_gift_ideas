from django.views.generic import ListView

from app.models import IdeaCategorie


class IdeaListWomenView(ListView):
    template_name = 'women.html'
    model = IdeaCategorie

    def get_queryset(self):
        return IdeaCategorie.objects.filter(categorie__description='Fête des mères')


