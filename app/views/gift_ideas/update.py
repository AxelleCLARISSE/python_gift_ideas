from django.urls import reverse
from django.views.generic import UpdateView

from app.forms.idea import IdeaForm
from app.models import Idea


class IdeaUpdateView(UpdateView):
    template_name = 'idea_update.html'
    model = Idea
    form_class = IdeaForm

    def get_success_url(self):
        return reverse('app_gift_ideas_list')
