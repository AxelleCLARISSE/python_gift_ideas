from django.conf import settings
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.views.generic import FormView

from app.forms.login import LoginForm


class LoginUserView(FormView):
    template_name = 'login.html'
    model = User
    form_class = LoginForm

    def get_success_url(self):
        return settings.LOGIN_REDIRECT_URL

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(self.request, username=username, password=password)
        if user is not None:
            login(self.request, user)
            return super().form_valid(form)
        form.add_error(None, 'user or password incorrect')
        return super().form_invalid(form)