from django.views.generic import ListView

from app.models import Idea


class IdeaTopListView(ListView):
    template_name = 'top.html'
    model = Idea

    def get_queryset(self):
        return Idea.objects.filter(note__note__gte=3).distinct();