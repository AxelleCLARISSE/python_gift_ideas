from django.views.generic import ListView

from app.models import Idea


class IdeaMyListView(ListView):
    template_name = 'activity.html'
    model = Idea

    def get_queryset(self):
        return Idea.objects.filter(person__user=self.request.user);