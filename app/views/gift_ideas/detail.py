from django.views.generic import DetailView

from app.models import Idea


class IdeaDetailView(DetailView):
    template_name = 'idea_detail.html'
    model = Idea
