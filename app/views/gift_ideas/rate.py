from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import CreateView

from app.forms.rate import RateForm
from app.models import Note, Person, Idea


class RateView(CreateView):
    template_name = 'idea_rate.html'
    model = Note
    form_class = RateForm

    def get_success_url(self):
        return reverse('app_index')

    def form_valid(self, form):
        person = Person.objects.get(user=self.request.user)
        note = Note.objects.create(note=form.cleaned_data['note'],
                                   idea_id=self.kwargs['pk'],
                                   person_id=person.id)
        note.save()

        return HttpResponseRedirect(self.get_success_url())
