from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import CreateView

from app.forms.register import RegisterForm
from app.models import Person


class RegisterView(CreateView):
    template_name = 'register.html'
    model = User
    form_class = RegisterForm

    def get_success_url(self):
        return reverse_lazy('app_index')

    def form_valid(self, form):
        user = User.objects.create_user(username=form.cleaned_data['username'],
                                        password=form.cleaned_data['password_1'],
                                        email=form.cleaned_data['email'],
                                        first_name=form.cleaned_data['first_name'],
                                        last_name=form.cleaned_data['last_name'])
        user.save()

        uploaded_file = self.request.FILES['avatar']
        fs = FileSystemStorage()
        name = fs.save(uploaded_file.name, uploaded_file)
        url = fs.url(name)
        person = Person.objects.create(user=user,
                                       avatar=url)
        person.save()

        return HttpResponseRedirect(self.get_success_url())
