from django.http import JsonResponse
from django.views import generic

from app.models import Idea
from app.views.gift_ideas import search


class IdeaSearchByDescriptionView(generic.ListView):
    template_name = 'activity.html'

    def get_queryset(self):
        search = self.kwargs['slug']
        return Idea.objects.filter(description__contains=search)

    def get(self, request, *args, **kwargs):
        tab = Idea.objects.filter(description__contains=search)
        return JsonResponse(tab, safe=False)
