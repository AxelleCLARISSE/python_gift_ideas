from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import CreateView

from app.forms.idea import IdeaForm
from app.models import Idea, Person, IdeaCategorie


class IdeaCreateView(CreateView):
    template_name = 'idea_create.html'
    model = Idea
    form_class = IdeaForm

    def get_success_url(self):
        return reverse('app_gift_ideas_list')

    def form_valid(self, form):
        person = Person.objects.get(user=self.request.user)
        idea = Idea.objects.create(title=form.cleaned_data['title'],
                                   description=form.cleaned_data['description'],
                                   prix=form.cleaned_data['prix'],
                                   photo=form.cleaned_data['photo'],
                                   person=person)
        idea.save()

        categorie = form.cleaned_data['categorie']
        ideaCategorie = IdeaCategorie.objects.create(idea=idea,
                                                     categorie=categorie[0])

        ideaCategorie.save()

        return HttpResponseRedirect(self.get_success_url())