from django.views.generic import ListView

from app.models import Idea


class IdeaSearchDetail(ListView):
    template_name = 'activity.html'
    model = Idea

    def get_queryset(self):
        title = self.kwargs.get('slug', '')
        return Idea.objects.filter(title__contains=title)
