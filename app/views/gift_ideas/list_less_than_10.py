from django.views.generic import ListView

from app.models import Idea


class IdeaListLessThan10View(ListView):
    template_name = 'idea_by_price.html'
    model = Idea

    def get_queryset(self):
        return Idea.objects.filter(prix__gte=5).filter(prix__lte=10).order_by('prix')


