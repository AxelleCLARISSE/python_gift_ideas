from django.views.generic import ListView

from app.models import Idea


class IdeaListGreaterThan100View(ListView):
    template_name = 'idea_by_price.html'
    model = Idea

    def get_queryset(self):
        return Idea.objects.filter(prix__gte=100).order_by('prix')


