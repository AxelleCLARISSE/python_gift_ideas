from django.contrib.auth.models import User
from django.urls import reverse
from django.views.generic import FormView

from app.forms.update_password import UpdatePasswordForm


class PasswordUpdateView(FormView):
    template_name = 'update_password.html'
    form_class = UpdatePasswordForm

    def get_success_url(self):
        return reverse('app_gift_ideas_profile')

    def form_valid(self, form):
        user = User.objects.get(username=self.request.user)
        user.set_password(form.cleaned_data['password_1'])
        user.save()

        return super().form_valid(form)
