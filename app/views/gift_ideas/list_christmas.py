from django.views.generic import ListView

from app.models import IdeaCategorie


class IdeaListChristmasView(ListView):
    template_name = 'christmas.html'
    model = IdeaCategorie

    def get_queryset(self):
        return IdeaCategorie.objects.filter(categorie__description='Noël')


