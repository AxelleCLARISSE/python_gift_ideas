from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.urls import reverse
from django.views.generic import FormView

from app.forms.profile import ProfileForm
from app.models import Person


class UserUpdateView(FormView):
    template_name = 'user_update.html'
    model = User
    form_class = ProfileForm

    def get_success_url(self):
        return reverse('app_gift_ideas_profile')

    def get_initial(self):
        initial = {
            'username': self.request.user.username,
            'first_name': self.request.user.first_name,
            'last_name': self.request.user.last_name,
            'email': self.request.user.email,
            'avatar': self.request.user.person.avatar
        }
        user = User.objects.get(username=self.request.user)
        user.username = ""
        user.save()

        return initial

    def form_valid(self, form):
        user = User.objects.get(username=self.request.user)

        user.username = form.cleaned_data['username']
        user.email = form.cleaned_data['email']
        user.first_name = form.cleaned_data['first_name']
        user.last_name = form.cleaned_data['last_name']

        user.save()

        person = Person.objects.get(user=user)

        if self.request.FILES:
            uploaded_file = self.request.FILES['avatar']
            fs = FileSystemStorage()
            if not fs.exists(uploaded_file.name):
                fs.save(uploaded_file.name, uploaded_file)

            url = fs.url(uploaded_file.name)

            person.avatar = url

        person.save()

        return super().form_valid(form)
