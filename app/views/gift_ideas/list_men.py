from django.views.generic import ListView

from app.models import IdeaCategorie


class IdeaListMenView(ListView):
    template_name = 'men.html'
    model = IdeaCategorie

    def get_queryset(self):
        return IdeaCategorie.objects.filter(categorie__description='Fête des pères')


