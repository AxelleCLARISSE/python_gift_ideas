from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView

from app.models import Idea


class IndexView(ListView):
    template_name = 'index.html'
    model = Idea

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)
        return result
