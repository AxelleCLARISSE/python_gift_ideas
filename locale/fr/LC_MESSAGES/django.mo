��    2      �  C   <      H  	   I     S     `     �     �     �     �     �     �     �  
   �     �  	   �     �               '     8     A     H     `     f  
   n     y  	   �     �     �  	   �  	   �     �     �     �     �     �     �     �     �     �     �     �       C     >   a  	   �     �     �     �     �     �  A  �  	        )  +   =     i     p     �     �     �     �     �     �     �     �     	     	     	     -	  	   H	  	   R	     \	     x	     ~	     �	  	   �	     �	     �	     �	  	   �	     �	     �	  	   �	     �	     �	     
  
    
     +
     8
     @
     G
     P
     d
  d   u
  A   �
          2     A     S     j     q     	         .   *             1   
   $   &   '                                                              -       +       /           "                  0      !   #   2                 %             (             )          ,          Action :  Activity Log Already have an account? Login! Back Back to details Back to list Back to my ideas Birthday Birthday category Cancel Categories Change password Christmas Christmas category Create Create an Account! Create new ideas Criteria Delete Different categories :  Email English First Name French Full Name Home Ideas Interface Last Name Log in again Login Logout Men Men category My ideas Password Price :  Profile Rate Rate the product Ready to Leave? Select "Logout" below if you are ready to end your current session. Thanks for spending some quality time with the Web site today. Top ideas Update Username Welcome Back! Women Women category Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Action :  Journal d'Activité Vous avez déjà un compte? Connectez vous! Retour Retour aux détails Retour à la liste Retour à mes idées Anniversaire Catégorie anniversaire Annuler Catégories Changer le mot de passe Noël Catégorie noël Créer Créer un Compte! Créer de nouvelles idées Critères Supprimer Différentes catégories :  Email Anglais Prénom Français Nom complet Accueil Idées Interface Nom de famille Connectez-vous à nouveau Connexion Déconnexion Hommes Catégorie fête des pères Mes idées Mot de passe Prix :  Profil Évaluez Évaluez le produit Prêt à partir? Sélectionnez "Déconnexion" ci-dessous si vous êtes prêt à mettre fin à votre session en cours. Merci pour le temps que vous avez accordé à ce site aujourd'hui Les meilleures idées Mettre à jour Nom d'utilisateur Bon retour parmi nous! Femmes Catégorie fête des mères 